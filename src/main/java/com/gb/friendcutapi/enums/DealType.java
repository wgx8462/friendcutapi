package com.gb.friendcutapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DealType {
    IN("받다"),
    OUT("주다");

    private final String name;
}
