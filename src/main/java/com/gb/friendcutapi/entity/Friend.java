package com.gb.friendcutapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Friend {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDate birtDay;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(nullable = false)
    private Boolean isCut;

    private LocalDate dateCut;

    @Column(length = 50)
    private String cutReason;
}
