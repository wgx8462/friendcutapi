package com.gb.friendcutapi.entity;

import com.gb.friendcutapi.enums.DealType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Heart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friendId", nullable = false)
    private Friend friend;

    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private DealType dealType;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate dateGift;
}
