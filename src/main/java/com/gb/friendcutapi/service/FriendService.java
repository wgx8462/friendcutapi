package com.gb.friendcutapi.service;

import com.gb.friendcutapi.entity.Friend;
import com.gb.friendcutapi.model.friend.FriendCreateRequest;
import com.gb.friendcutapi.model.friend.FriendCutUpdateRequest;
import com.gb.friendcutapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public void setFriend(FriendCreateRequest request) {
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirtDay(request.getBirtDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCut(false);

        friendRepository.save(addData);
    }



    public void putFriendCut(long id, FriendCutUpdateRequest request) {
        Friend originDate = friendRepository.findById(id).orElseThrow();
        originDate.setIsCut(true);
        originDate.setDateCut(LocalDate.now());
        originDate.setCutReason(request.getCutReason());

        friendRepository.save(originDate);
    }
}
