package com.gb.friendcutapi.service;

import com.gb.friendcutapi.repository.HeartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HeartService {
    private final HeartRepository heartRepository;

    public void delHeart(long id) {
        heartRepository.deleteById(id);
    }

}
