package com.gb.friendcutapi.repository;

import com.gb.friendcutapi.entity.Heart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeartRepository extends JpaRepository<Heart, Long> {
}
