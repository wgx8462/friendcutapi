package com.gb.friendcutapi.repository;

import com.gb.friendcutapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
