package com.gb.friendcutapi.controller;

import com.gb.friendcutapi.model.friend.FriendCreateRequest;
import com.gb.friendcutapi.model.friend.FriendCutUpdateRequest;
import com.gb.friendcutapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/vi/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/join")
    public String setFriend(@RequestBody FriendCreateRequest request) {
        friendService.setFriend(request);

        return "등록 완료";
    }

    @PutMapping("/cut/{id}")
    public String putFriendCut(@PathVariable long id, @RequestBody FriendCutUpdateRequest request) {
        friendService.putFriendCut(id, request);

        return "수정 완료";
    }
}
