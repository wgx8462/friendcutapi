package com.gb.friendcutapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCutUpdateRequest {
    private String cutReason;
}
